#!/bin/sh

FILES=`find . -path ./plugin -prune -type f -o -name "*.lua"`

function format_files {
  for file in $FILES;
  do
    lua-format -i $file
  done
}

function show_diff {
  for file in $FILES;
  do
    lua-format $file | diff -y --color=always - $file
  done
}

format_files

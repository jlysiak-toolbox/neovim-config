# Neovim dots

Neovim configuration package written in `lua`.

Based on a great tutorial from [Chris@Machine](#chris-at-machine), [Neovim from scratch](#nvim-from-scratch).

I really recommend it, so check it out if you want to understand how to configure your neovim.


[chris-at-machine]:https://www.youtube.com/channel/UCS97tchJDq17Qms3cux8wcA
[nvim-from-scratch]:https://www.youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ

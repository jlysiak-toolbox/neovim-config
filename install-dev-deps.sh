#!/bin/sh


# Get LuaFormatter
function formatter_install_luarocks {
    luarocks install --server=https://luarocks.org/dev luaformatter
}

function formatter_install_git {
    DIR=~/.local/src/luaformatter
    git clone --recurse-submodules https://github.com/Koihik/LuaFormatter.git $DIR
    cd $DIR
    cmake .
    make
    make install
}

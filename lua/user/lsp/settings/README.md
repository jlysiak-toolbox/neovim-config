# Language server configurations directory

Check [server_configurations.md] for available language server configurations.

[server_configs]:https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md

local log = print
local name = 'lspconfig'

local status_ok, _ = pcall( require, name )
if not status_ok then
  log( 'Can\'t find: ' .. name )
  return
end

require( 'user.lsp.lsp-installer' )
require( 'user.lsp.handlers' ).setup()
require( 'user.lsp.null-ls' )
